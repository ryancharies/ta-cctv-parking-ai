import cv2
import tensorflow as tf
import numpy as np
import tensorflow_hub as hub
from tensorflow.keras.models import load_model
from tensorflow.keras.preprocessing.image import img_to_array, load_img
from keras_preprocessing.image import load_img, img_to_array
from tensorflow.keras.applications.mobilenet_v2 import preprocess_input
from skimage.transform import resize
from pygame import mixer
from flask import Flask, render_template, Response
from pyngrok import ngrok
import firebase_admin
from firebase_admin import db
import RPi.GPIO as GPIO
import requests
from time import sleep

# Mendefinisikan Firebase
cred_obj = firebase_admin.credentials.Certificate('ta-cctv-parking-firebase-adminsdk-sv5mr-49cfc8fe4a.json')
default_app = firebase_admin.initialize_app(cred_obj, {
	'databaseURL': 'https://ta-cctv-parking-default-rtdb.firebaseio.com'
})
ngrok_url_ref = db.reference("/ngrok_url")

# Mendefinisikan Flask
app = Flask(__name__)

@app.route('/')
def index():
    """Video streaming home page."""
    return render_template('index.html')

@app.route('/video_feed')
def video_feed():
    """Video streaming route. Put this in the src attribute of an img tag."""
    return Response(gen(), mimetype='multipart/x-mixed-replace; boundary=frame')

# Mendefinisikan Sound
mixer.init()
snd1 = mixer.Sound('./sound/warning.mp3')
snd2 = mixer.Sound('./sound/warning.mp3')
snd3 = mixer.Sound('./sound/warning-motor.mp3')
sound_denied_1 = mixer.Sound('./sound/warning-motor.mp3')
sound_warning_1 = mixer.Sound('./sound/warning.mp3')
sound_denied_2 = mixer.Sound('./sound/warning-motor.mp3')
sound_warning_2 = mixer.Sound('./sound/warning.mp3')

#booking parkir (0: tidak dibooking dan 1: dibooking)
park1 = 1 #area parkir sebelah kiri
park2 = 0 #area parkir sebelah kanan

# Mendefinisikan Raspi
btnAOpen = 23
btnAClose = 24
btnBOpen = 25
btnBClose = 12
relayA = 5
relayB = 6

def btnOpen_callback(channel):
    global park1
    print("Button A Open was pushed!")
    url = 'http://mdthosting.com/cctvparking/public/callback/remote'
    myobj = {'is_active': 1, 'token_remote': 'XtggHAX1GU'}
    res = requests.post(url, json = myobj)
    print(res.text)
    GPIO.output(relayA, GPIO.HIGH)
    park1 = 1

def btnClose_callback(channel):
    global park1
    print("Button A Close was pushed!")
    url = 'http://mdthosting.com/cctvparking/public/callback/remote'
    myobj = {'is_active': 0, 'token_remote': 'XtggHAX1GU'}
    res = requests.post(url, json = myobj)
    print(res.text)
    GPIO.output(relayA, GPIO.LOW)
    park1 = 0
    
def btnOpenB_callback(channel):
    global park2
    print("Button B Open was pushed!")
    url = 'http://mdthosting.com/cctvparking/public/callback/remote'
    myobj = {'is_active': 1, 'token_remote': 'o31pZqlE1y'}
    res = requests.post(url, json = myobj)
    print(res.text)
    GPIO.output(relayB, GPIO.HIGH)
    park2 = 1

def btnCloseB_callback(channel):
    global park2
    print("Button B Close was pushed!")
    url = 'http://mdthosting.com/cctvparking/public/callback/remote'
    myobj = {'is_active': 0, 'token_remote': 'o31pZqlE1y'}
    res = requests.post(url, json = myobj)
    print(res.text)
    GPIO.output(relayB, GPIO.LOW)
    park2 = 0

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.setup(btnAOpen, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(btnAClose, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(btnBOpen, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(btnBClose, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(relayA, GPIO.OUT)
GPIO.setup(relayB, GPIO.OUT)

GPIO.add_event_detect(btnAOpen,GPIO.RISING,callback=btnOpen_callback)
GPIO.add_event_detect(btnAClose,GPIO.RISING,callback=btnClose_callback)
GPIO.add_event_detect(btnBOpen,GPIO.RISING,callback=btnOpenB_callback)
GPIO.add_event_detect(btnBClose,GPIO.RISING,callback=btnCloseB_callback)

# Mendefinisikan model TensorFlow Hub MODEL1
model_path = './model_parkir_1.h5'
model = load_model(model_path, custom_objects={'KerasLayer': hub.KerasLayer}, compile=False)
model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])

# Mendefinisikan model TensorFlow Hub MODEL2
model_path = './model_parkir_2.h5'
model2 = load_model(model_path, custom_objects={'KerasLayer': hub.KerasLayer}, compile=False)
model2.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])

# Membaca mask image
mask_image_path = './mask1.png'
mask = cv2.imread(mask_image_path, 0)

# Membaca video
video_path = 'video/pb2_hd.mp4'
cap = cv2.VideoCapture(video_path)

def get_parking_spots_bboxes(connected_components):
    (totalLabels, label_ids, values, centroid) = connected_components

    slots = []
    coef = 1
    for i in range(1, totalLabels):

        # Now extract the coordinate points
        x1 = int(values[i, cv2.CC_STAT_LEFT] * coef)
        y1 = int(values[i, cv2.CC_STAT_TOP] * coef)
        w = int(values[i, cv2.CC_STAT_WIDTH] * coef)
        h = int(values[i, cv2.CC_STAT_HEIGHT] * coef)

        slots.append([x1, y1, w, h])

    return slots

def preprocess_frame(frame):
    frame = cv2.resize(frame, (224, 224))  # Resize the frame to match model input size
    frame = img_to_array(frame)
    frame = preprocess_input(frame)
    frame = np.expand_dims(frame, axis=0)  # Add a batch dimension
    return frame

def predict_image(image_path):
    img = preprocess_frame(image_path)
    predictions = model.predict(img)
    class_labels = ["Kosong", "Mobil", "Motor", "Orang"]
    predicted_label = class_labels[np.argmax(predictions)]
    confidence = np.max(predictions)
    return predicted_label, confidence

def model_parkir_2(image):
    img = preprocess_frame(image)
    predictions = model2.predict(img)
    class_labels = ["benar","tidak_benar"]
    predicted_label = class_labels[np.argmax(predictions)]
    confidence = np.max(predictions)

    return predicted_label, confidence

# Mendapatkan bounding box dari mask image
connected_components = cv2.connectedComponentsWithStats(mask, 4, cv2.CV_32S)
spots = get_parking_spots_bboxes(connected_components)

# frame_nmr = 0
ret = True
# step = 30
# previous_frame = None

def gen():
    while(cap.isOpened()):
        ret, frame = cap.read()
        if frame is not None:
            for spot in spots:
                x1, y1, w, h = spot
                spot_crop = frame[y1:y1 + h, x1:x1 + w, :]
                predicted_label, confidence = predict_image(spot_crop)

                #park1 kiri
                if spot[0] == 19:
                    # Marking Alphabet
                    cv2.putText(frame, 'A', (250, 350), cv2.FONT_HERSHEY_SIMPLEX, 4, (255, 255, 255), 4)

                    if predicted_label == "Motor" and confidence >= 0.95:
                        frame = cv2.rectangle(frame, (x1,y1), (x1 + w, y1 + h), (0,0,255),2) #red
                        cv2.putText(frame, 'Ada motor ? atau sesuatu', (100, 150), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2)
                        sound_denied_1.play()
                    elif predicted_label == "Mobil" and confidence >= 0.95:
                        #booking parkir
                        if park1 == 1:
                            # frame = cv2.rectangle(frame, (x1,y1), (x1 + w, y1 + h), (0,0,255),2) #red
                            # cv2.putText(frame, 'Lahan parkir sudah dipesan', (100, 500), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2)

                            frame = cv2.rectangle(frame, (x1,y1), (x1 + w, y1 + h), (0,255,0),2) #green
                            
                            labelnya, kemungkinan = model_parkir_2(spot_crop)

                            if kemungkinan > 0.020:
                                cv2.putText(frame, 'Parkir Tidak Benar(L)', (100, 100),
                                cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2)
                                sound_warning_1.play()
                            else:
                                cv2.putText(frame, 'Parkir Benar(L)', (100, 100),
                                cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2)
                                sound_warning_1.stop()

                        else:
                            frame = cv2.rectangle(frame, (x1,y1), (x1 + w, y1 + h), (0,0,255),2) #red
                            sound_denied_1.play()

                    else:
                        frame = cv2.rectangle(frame, (x1,y1), (x1 + w, y1 + h), (255,0,0),2) #blue
                        sound_warning_1.stop()
                        sound_denied_1.stop()

                #parkir kanan
                elif spot[0] == 665:
                    # Marking Alphabet
                    cv2.putText(frame, 'B', (950, 350), cv2.FONT_HERSHEY_SIMPLEX, 4, (255, 255, 255), 4)
                    if predicted_label == "Motor" and confidence >= 0.90:
                        frame = cv2.rectangle(frame, (x1,y1), (x1 + w, y1 + h), (0,0,255),2) #red
                        cv2.putText(frame, 'Ada motor ? atau sesuatu', (800, 150), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2)
                        sound_denied_2.play()
                    
                    elif predicted_label == "Mobil" and confidence >= 0.90:
                        frame = cv2.rectangle(frame, (x1,y1), (x1 + w, y1 + h), (0,255,0),2) #green

                        #booking parkir
                        if park2 == 1:
                            # frame = cv2.rectangle(frame, (x1,y1), (x1 + w, y1 + h), (0,0,255),2) #red
                            # cv2.putText(frame, 'Lahan parkir sudah dipesan', (800, 500), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2)

                            labelnya, kemungkinan = model_parkir_2(spot_crop)

                            if kemungkinan > 0.020:
                                cv2.putText(frame, 'Parkir Tidak Benar(R)', (900, 100),
                                cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2)
                                sound_warning_2.play()
                            else:
                                cv2.putText(frame, 'Parkir Benar(R)', (900, 100),
                                cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2)
                                sound_warning_2.stop()
                        else:
                            frame = cv2.rectangle(frame, (x1,y1), (x1 + w, y1 + h), (0,0,255),2) #red
                            sound_denied_2.play()

                    else:
                        frame = cv2.rectangle(frame, (x1,y1), (x1 + w, y1 + h), (255,0,0),2) #blue
                        sound_denied_2.stop()
                        sound_warning_2.stop()
                else:
                    break

            # cv2.namedWindow('frame', cv2.WINDOW_NORMAL)
            # cv2.imshow('frame', frame)

            imageFlask = cv2.resize(frame, (0,0), fx=0.5, fy=0.5) 
            frameFlask = cv2.imencode('.jpg', imageFlask)[1].tobytes()
            yield (b'--frame\r\n'b'Content-Type: image/jpeg\r\n\r\n' + frameFlask + b'\r\n')
            # if cv2.waitKey(25) & 0xFF == ord('q'):
            #     break
  
if __name__ == '__main__':
    ngrok_url = ngrok.connect(8080).public_url
    print('Ngrok URL:', ngrok_url)
    ngrok_url_ref.set(ngrok_url)
    app.run(port=8080)

# cap.release()
# cv2.destroyAllWindows()
